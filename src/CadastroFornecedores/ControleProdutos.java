
package CadastroFornecedores;

import java.util.ArrayList;

public class ControleProdutos {
    
    private ArrayList<Produto> listaProdutos;
    
    public ControleProdutos(){
       listaProdutos = new ArrayList<Produto>();
    }
    
    public String adicionar(Produto mercadoria){
            listaProdutos.add(mercadoria);
            return "Produto adicionado com sucesso!";
        }
         
          public String remover(Produto mercadoria){
            listaProdutos.remove(mercadoria);
            return "Produto removido com sucesso!";
        }
         
         public Produto pesquisar(String umProduto){
		for(Produto mercadoria: listaProdutos){
			if(mercadoria.getNome().equalsIgnoreCase(umProduto)){
				return mercadoria;
			}
		}
		return null;
		}
    
    
}
