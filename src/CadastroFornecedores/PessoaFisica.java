
package CadastroFornecedores;

public class PessoaFisica extends Fornecedor {
    
    private String cpf;
    
    public PessoaFisica(String nome, String cpf){
        
       super(nome);
       this.cpf = cpf;
       this.ident = "PessoaFisica";
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
    
    
    
}
