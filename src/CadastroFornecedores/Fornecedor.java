
package CadastroFornecedores;

import java.util.ArrayList;


public class Fornecedor {
    
    private String nome;
    private ArrayList<String> telefones;
    private Endereco endereco;
    private ArrayList<Produto> produtos;
    protected String ident;
    
    public Fornecedor (String nome){
        this.nome = nome;
        this.ident = "Fornecedor";
        }
    
    public String getIdent(){
        return ident;
    }
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public ArrayList<String> getTelefones() {
        return telefones;
    }

    public void setTelefones(ArrayList<String> telefones) {
        this.telefones = telefones;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public ArrayList<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(ArrayList<Produto> produtos){
        this.produtos = produtos;
    }
   
}