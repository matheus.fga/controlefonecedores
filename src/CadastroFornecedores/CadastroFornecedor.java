

package CadastroFornecedores;

import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

public class CadastroFornecedor extends javax.swing.JFrame {

    private final byte PESSOA_FISICA_INDICE = 1;
    private final byte PESSOA_JURIDICA_INDICE = 2;
    private DefaultListModel telefonesListModel;
    private DefaultListModel produtosListModel;
    private boolean novoRegistro;
    private boolean modoAlteracao;
    private ControleFornecedores controleFornecedor;
    private ControleProdutos controleProduto;
    private Fornecedor umFornecedor;
    private PessoaFisica umaPessoaFisica;
    private PessoaJuridica umaPessoaJuridica;
    private Produto umProduto;
  
    
    public CadastroFornecedor() {
        initComponents();
        this.habilitarDesabilitarCampos();
        this.controleFornecedor = new ControleFornecedores();
        this.telefonesListModel = new DefaultListModel();
        this.jListTelefones.setModel(telefonesListModel);
        this.produtosListModel = new DefaultListModel();
        this.jListProdutos.setModel(produtosListModel);
        this.jTableListaFornecedor.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }
    
     private void limparCampos() {
         
        jTextFieldValorCompra.setText("0.00");
        jTextFieldBairro.setText(null);
        jTextFieldNomeProduto.setText(null);
        jTextFieldRazaoSocial.setText(null);
        jTextFieldCidade.setText(null);
        jTextFieldComplemento.setText(null);
        jTextFieldCpf.setText(null);
        jTextFieldQuantidade.setText("0");
        jTextFieldValorVenda.setText("0.0");
        jComboBoxEstado.setSelectedIndex(0);
        jTextFieldLogradouro.setText(null);
        jTextFieldNome.setText(null);
        jTextFieldCnpj.setText(null);
        jTextFieldDescricao.setText(null);
        jTextFieldNumero.setText("0");
        jTextFieldPais.setText(null); 
        jTextFieldNomeProduto.setText(null);
        telefonesListModel.clear();
        produtosListModel.clear();
        jComboBoxFornecedor.setSelectedIndex(0);
       
    }
     
     private void preencherCampos() {
        ArrayList<String> telefones;
        ArrayList<Produto> produtos;
        
        if(umFornecedor.getIdent().equals("PessoaFisica")) {
            
                    PessoaFisica pessoaFisica = (PessoaFisica) umFornecedor;    
                    
                    jTextFieldBairro.setText(pessoaFisica.getEndereco().getBairro());
                    jTextFieldRazaoSocial.setText("");
                    jTextFieldCidade.setText(pessoaFisica.getEndereco().getCidade());
                    jTextFieldComplemento.setText(pessoaFisica.getEndereco().getComplemento());
                    jTextFieldCpf.setText(pessoaFisica.getCpf());
                    jTextFieldCnpj.setText("");
                    jComboBoxEstado.setSelectedItem(pessoaFisica.getEndereco().getEstado());
                    jTextFieldLogradouro.setText(pessoaFisica.getEndereco().getLogradouro());
                    jTextFieldNome.setText(pessoaFisica.getNome());
                    jTextFieldNumero.setText(pessoaFisica.getEndereco().getNumero());
                    jTextFieldPais.setText(pessoaFisica.getEndereco().getPais());

                    telefonesListModel.clear();
                    telefones = pessoaFisica.getTelefones();
                    for (String t : telefones) {
                        telefonesListModel.addElement(t);
                    }
                    
                    produtosListModel.clear();
                    produtos = pessoaFisica.getProdutos();
                    for (Produto p : produtos) {
                        produtosListModel.addElement(p);
                    }
                    
                    jComboBoxFornecedor.setSelectedIndex(PESSOA_FISICA_INDICE);
        }
        
        else {
                    PessoaJuridica pessoaJuridica = (PessoaJuridica) umFornecedor;
                    
                    jTextFieldBairro.setText(pessoaJuridica.getEndereco().getBairro());
                    jTextFieldRazaoSocial.setText(pessoaJuridica.getRazaoSocial());
                    jTextFieldCidade.setText(pessoaJuridica.getEndereco().getCidade());
                    jTextFieldComplemento.setText(pessoaJuridica.getEndereco().getComplemento());
                    jTextFieldCpf.setText("");
                    jTextFieldCnpj.setText(pessoaJuridica.getCnpj());
                    jComboBoxEstado.setSelectedItem(pessoaJuridica.getEndereco().getEstado());
                    jTextFieldLogradouro.setText(pessoaJuridica.getEndereco().getLogradouro());
                    jTextFieldNome.setText(pessoaJuridica.getNome());
                    jTextFieldNumero.setText(pessoaJuridica.getEndereco().getNumero());
                    jTextFieldPais.setText(pessoaJuridica.getEndereco().getPais());

                    telefonesListModel.clear();
                    telefones = pessoaJuridica.getTelefones();
                    for (String t : telefones) {
                        telefonesListModel.addElement(t);
                    }
                    
                    produtosListModel.clear();
                    produtos = pessoaJuridica.getProdutos();
                    for (Produto p : produtos) {
                        produtosListModel.addElement(p);
                    }
                    
                     jComboBoxFornecedor.setSelectedIndex(PESSOA_JURIDICA_INDICE);
        }
     }
     
     
     private boolean validarCampos() {
        if (jTextFieldNome.getText().trim().length() == 0) {
            this.exibirInformacao("O valor do campo 'Nome' não foi informado.");
            jTextFieldNome.requestFocus();
            return false;
        }
        return true;
     }
     
     
     
     private void habilitarDesabilitarCampos() {
        boolean registroSelecionado = (umFornecedor != null);
        
        jComboBoxFornecedor.setEnabled(modoAlteracao);
        jTextFieldNome.setEnabled(modoAlteracao);
        jTextFieldCpf.setEnabled(modoAlteracao);
        jTextFieldCnpj.setEnabled(modoAlteracao);
        jTextFieldRazaoSocial.setEnabled(modoAlteracao);
        jTextFieldLogradouro.setEnabled(modoAlteracao);
        jTextFieldNumero.setEnabled(modoAlteracao);
        jTextFieldBairro.setEnabled(modoAlteracao);
        jTextFieldCidade.setEnabled(modoAlteracao);
        jComboBoxEstado.setEnabled(modoAlteracao);
        jTextFieldPais.setEnabled(modoAlteracao);
        jTextFieldComplemento.setEnabled(modoAlteracao);
        jTextFieldNomeProduto.setEnabled(modoAlteracao);
        jTextFieldDescricao.setEnabled(modoAlteracao);
        jTextFieldValorCompra.setEnabled(modoAlteracao);
        jTextFieldValorVenda.setEnabled(modoAlteracao);
        jTextFieldQuantidade.setEnabled(modoAlteracao);
        jButtonCancelar.setEnabled(modoAlteracao);
        jButtonExcluir.setEnabled(modoAlteracao == false && registroSelecionado == true);
        jButtonNovo.setEnabled(modoAlteracao == false);
        jButtonPesquisar.setEnabled(modoAlteracao == false);
        jButtonEditar.setEnabled(modoAlteracao == false && registroSelecionado == true);
        jButtonSalvar.setEnabled(modoAlteracao);
        jTableListaFornecedor.setEnabled(modoAlteracao == false);
        jButtonAdicionar.setEnabled(modoAlteracao);
        jButtonRemover.setEnabled(modoAlteracao);
        jButtonAdicionarProduto.setEnabled(modoAlteracao);
        jButtonRemoverProduto.setEnabled(modoAlteracao);
    }
     
     private void salvarRegistro(){
        
        Endereco endereco = new Endereco();
        Produto produto = new Produto();
        ArrayList<String> telefones;
        ArrayList<Produto> produtos;
        
        
        if (this.validarCampos() == false) {
            return;   
        }
            
           switch (jComboBoxFornecedor.getSelectedIndex()) {
               
              case PESSOA_FISICA_INDICE:
                  
                if (novoRegistro == true) {
                     umaPessoaFisica = new PessoaFisica(jTextFieldNome.getText(), jTextFieldCpf.getText());
                } else {
                   umaPessoaFisica.setNome(jTextFieldNome.getText());
                   umaPessoaFisica.setCpf(jTextFieldCpf.getText());
                  }  
                  
                endereco.setLogradouro(jTextFieldLogradouro.getText());
                endereco.setNumero(jTextFieldNumero.getText());
                endereco.setBairro(jTextFieldBairro.getText());
                endereco.setCidade(jTextFieldCidade.getText());
                endereco.setPais(jTextFieldPais.getText());
                endereco.setComplemento(jTextFieldComplemento.getText());

                umaPessoaFisica.setEndereco(endereco);

                produto.setNome(jTextFieldNomeProduto.getText());
                produto.setDescricao(jTextFieldDescricao.getText());

                Double valorCompraDouble = jTextFieldValorCompra.getText().length() == 0 ? 0 : Double.parseDouble(jTextFieldValorCompra.getText());
                produto.setValorCompra(valorCompraDouble);

                Double valorVendaDouble = jTextFieldValorVenda.getText().length() == 0 ? 0 : Double.parseDouble(jTextFieldValorVenda.getText());
                produto.setValorVenda(valorVendaDouble);

                Double valorQuantidadeEstoqueDouble = jTextFieldQuantidade.getText().length() == 0 ? 0 : Double.parseDouble(jTextFieldQuantidade.getText());
                produto.setQuantidadeEstoque(valorQuantidadeEstoqueDouble);


                telefones = new ArrayList<String>();
                for (int i = 0; i < telefonesListModel.size(); i++) {
                    telefones.add(telefonesListModel.getElementAt(i).toString());
                }
                
                umaPessoaFisica.setTelefones(telefones);
             
                 produtos = new ArrayList<Produto>();
                for (int i = 0; i < produtosListModel.size(); i++) {
                   produto  = (Produto) produtosListModel.getElementAt(i);
                    produtos.add(produto);
                }
                
                umaPessoaFisica.setProdutos(produtos);
                
                break;
            
              case PESSOA_JURIDICA_INDICE:
                  
                 if (novoRegistro == true) {
                     umaPessoaJuridica = new PessoaJuridica(jTextFieldNome.getText(), jTextFieldCnpj.getText());
                } else {
                   umaPessoaJuridica.setNome(jTextFieldNome.getText());
                   umaPessoaJuridica.setCnpj(jTextFieldCnpj.getText());
                  }  
                  
                endereco.setLogradouro(jTextFieldLogradouro.getText());
                endereco.setNumero(jTextFieldNumero.getText());
                endereco.setBairro(jTextFieldBairro.getText());
                endereco.setCidade(jTextFieldCidade.getText());
                endereco.setPais(jTextFieldPais.getText());
                endereco.setComplemento(jTextFieldComplemento.getText());

                umaPessoaJuridica.setEndereco(endereco);

                produto.setNome(jTextFieldNomeProduto.getText());
                produto.setDescricao(jTextFieldDescricao.getText());

                valorCompraDouble = jTextFieldValorCompra.getText().length() == 0 ? 0 : Double.parseDouble(jTextFieldValorCompra.getText());
                produto.setValorCompra(valorCompraDouble);

                valorVendaDouble = jTextFieldValorVenda.getText().length() == 0 ? 0 : Double.parseDouble(jTextFieldValorVenda.getText());
                produto.setValorVenda(valorVendaDouble);

                valorQuantidadeEstoqueDouble = jTextFieldQuantidade.getText().length() == 0 ? 0 : Double.parseDouble(jTextFieldQuantidade.getText());
                produto.setQuantidadeEstoque(valorQuantidadeEstoqueDouble);


                telefones = new ArrayList<String>();
                for (int i = 0; i < telefonesListModel.size(); i++) {
                    telefones.add(telefonesListModel.getElementAt(i).toString());
                }
              
                 umaPessoaJuridica.setTelefones(telefones);
                 
                 produtos = new ArrayList<Produto>();
                for (int i = 0; i < produtosListModel.size(); i++) {
                   produto  = (Produto) produtosListModel.getElementAt(i);
                    produtos.add(produto);
                }
                
                 umaPessoaJuridica.setProdutos(produtos);
                 
                break; 
            
            }
            
        
        if (novoRegistro == true) {
            
            switch (jComboBoxFornecedor.getSelectedIndex()) {
            case PESSOA_FISICA_INDICE:
                controleFornecedor.adicionar(umaPessoaFisica);
                break;
            case PESSOA_JURIDICA_INDICE:
               controleFornecedor.adicionar(umaPessoaJuridica);
                break;
            }
            
        }
        
        modoAlteracao = false;
        novoRegistro = false;
        
        this.carregarListaFornecedores();
        this.habilitarDesabilitarCampos();
       
    }

    private void exibirInformacao(String info) {
        JOptionPane.showMessageDialog(this, info, "Atenção", JOptionPane.INFORMATION_MESSAGE);
    }
    
     private void carregarListaFornecedores(){
        ArrayList<Fornecedor> listaFornecedores;
        listaFornecedores = controleFornecedor.getListaFornecedor();
        DefaultTableModel model = (DefaultTableModel) jTableListaFornecedor.getModel();
        model.setRowCount(0);

        for(Fornecedor fornecedor : listaFornecedores){
                if(fornecedor.getIdent().equals("PessoaFisica")) {
                    PessoaFisica pessoaFisica = (PessoaFisica) fornecedor;    
                    model.addRow(new String[]{pessoaFisica.getNome(),  pessoaFisica.getCpf() + "(CPF)"});
                } else {
                    PessoaJuridica pessoaJuridica = (PessoaJuridica) fornecedor;
                    model.addRow(new String[]{pessoaJuridica.getNome(), pessoaJuridica.getCnpj() + "(CNPJ)"});
                }
        }
        
        jTableListaFornecedor.setModel(model);
    }
    
      private void pesquisarFornecedor(String nome) {
        Fornecedor fornecedorPesquisado = controleFornecedor.pesquisar(nome);

        if (fornecedorPesquisado == null) {
            exibirInformacao("Fornecedor não encontrado.");
        } else {
            this.umFornecedor= fornecedorPesquisado;
            this.preencherCampos();
            this.habilitarDesabilitarCampos();
        }
    }
    
     
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButtonNovo = new javax.swing.JButton();
        jButtonExcluir = new javax.swing.JButton();
        jButtonPesquisar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableListaFornecedor = new javax.swing.JTable();
        jLabelListaFornecedores = new javax.swing.JLabel();
        jTabbedInformações = new javax.swing.JTabbedPane();
        jPanelInformaçõesGerais = new javax.swing.JPanel();
        jComboBoxFornecedor = new javax.swing.JComboBox();
        jLabelTipoFornecedor = new javax.swing.JLabel();
        jLabelNome = new javax.swing.JLabel();
        jLabelCpf = new javax.swing.JLabel();
        jTextFieldNome = new javax.swing.JTextField();
        jTextFieldCpf = new javax.swing.JTextField();
        jLabelCnpj = new javax.swing.JLabel();
        jTextFieldCnpj = new javax.swing.JTextField();
        RazaoSocial = new javax.swing.JLabel();
        jTextFieldRazaoSocial = new javax.swing.JTextField();
        jLabelTelefones = new javax.swing.JLabel();
        jButtonAdicionar = new javax.swing.JButton();
        jButtonRemover = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jListTelefones = new javax.swing.JList();
        jPanelEndereco = new javax.swing.JPanel();
        JLabelLogradouro = new javax.swing.JLabel();
        jTextFieldLogradouro = new javax.swing.JTextField();
        jLabelNumero = new javax.swing.JLabel();
        jTextFieldNumero = new javax.swing.JTextField();
        jLabelBairro = new javax.swing.JLabel();
        jTextFieldBairro = new javax.swing.JTextField();
        jLabelCidade = new javax.swing.JLabel();
        jTextFieldCidade = new javax.swing.JTextField();
        jLabelEstado = new javax.swing.JLabel();
        jTextFieldPais = new javax.swing.JTextField();
        jLabelPais = new javax.swing.JLabel();
        jComboBoxEstado = new javax.swing.JComboBox();
        jLabelComplemento = new javax.swing.JLabel();
        jTextFieldComplemento = new javax.swing.JTextField();
        jPanelProduto = new javax.swing.JPanel();
        jLabelNomeProduto = new javax.swing.JLabel();
        jTextFieldNomeProduto = new javax.swing.JTextField();
        jLabelValorCompra = new javax.swing.JLabel();
        jTextFieldValorCompra = new javax.swing.JTextField();
        jLabelValorVenda = new javax.swing.JLabel();
        jTextFieldValorVenda = new javax.swing.JTextField();
        jLabelQuantidade = new javax.swing.JLabel();
        jTextFieldQuantidade = new javax.swing.JTextField();
        jLabelDescricao = new javax.swing.JLabel();
        jTextFieldDescricao = new javax.swing.JTextField();
        jLabelListaProdutos = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jListProdutos = new javax.swing.JList();
        jButtonAdicionarProduto = new javax.swing.JButton();
        jButtonRemoverProduto = new javax.swing.JButton();
        jButtonSalvar = new javax.swing.JButton();
        jButtonCancelar = new javax.swing.JButton();
        jButtonEditar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jButtonNovo.setText("Novo");
        jButtonNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNovoActionPerformed(evt);
            }
        });

        jButtonExcluir.setText("Excluir");
        jButtonExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonExcluirActionPerformed(evt);
            }
        });

        jButtonPesquisar.setText("Pesquisar");
        jButtonPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPesquisarActionPerformed(evt);
            }
        });

        jTableListaFornecedor.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Nome", "CPF/CNPJ"
            }
        ));
        jTableListaFornecedor.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableListaFornecedorMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTableListaFornecedor);

        jLabelListaFornecedores.setText("Lista de Fornecedores");

        jPanelInformaçõesGerais.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0))));

        jComboBoxFornecedor.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "<Selecione>", "Pessoa Física", "Pessoa Jurídica", " " }));
        jComboBoxFornecedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxFornecedorActionPerformed(evt);
            }
        });

        jLabelTipoFornecedor.setText("Tipo de Fornecedor:");

        jLabelNome.setText("Nome:");

        jLabelCpf.setText("CPF:");

        jLabelCnpj.setText("CNPJ:");

        RazaoSocial.setText("Razão Social:");

        jLabelTelefones.setText("Telefones:");

        jButtonAdicionar.setText("+");
        jButtonAdicionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAdicionarActionPerformed(evt);
            }
        });

        jButtonRemover.setText("-");
        jButtonRemover.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonRemoverActionPerformed(evt);
            }
        });

        jScrollPane2.setViewportView(jListTelefones);

        javax.swing.GroupLayout jPanelInformaçõesGeraisLayout = new javax.swing.GroupLayout(jPanelInformaçõesGerais);
        jPanelInformaçõesGerais.setLayout(jPanelInformaçõesGeraisLayout);
        jPanelInformaçõesGeraisLayout.setHorizontalGroup(
            jPanelInformaçõesGeraisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelInformaçõesGeraisLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelInformaçõesGeraisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelInformaçõesGeraisLayout.createSequentialGroup()
                        .addComponent(jLabelTipoFornecedor)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboBoxFornecedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanelInformaçõesGeraisLayout.createSequentialGroup()
                        .addGroup(jPanelInformaçõesGeraisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelInformaçõesGeraisLayout.createSequentialGroup()
                                .addComponent(jLabelNome)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextFieldNome, javax.swing.GroupLayout.PREFERRED_SIZE, 423, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanelInformaçõesGeraisLayout.createSequentialGroup()
                                .addComponent(jLabelCpf)
                                .addGap(27, 27, 27)
                                .addComponent(jTextFieldCpf, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelCnpj)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextFieldCnpj, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanelInformaçõesGeraisLayout.createSequentialGroup()
                                .addComponent(RazaoSocial)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextFieldRazaoSocial, javax.swing.GroupLayout.PREFERRED_SIZE, 380, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanelInformaçõesGeraisLayout.createSequentialGroup()
                                .addComponent(jLabelTelefones)
                                .addGap(24, 24, 24)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 551, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanelInformaçõesGeraisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jButtonAdicionar, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jButtonRemover, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(0, 51, Short.MAX_VALUE))))
        );
        jPanelInformaçõesGeraisLayout.setVerticalGroup(
            jPanelInformaçõesGeraisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelInformaçõesGeraisLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelInformaçõesGeraisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBoxFornecedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelTipoFornecedor))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelInformaçõesGeraisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelNome)
                    .addComponent(jTextFieldNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelInformaçõesGeraisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelCpf)
                    .addComponent(jTextFieldCpf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelCnpj)
                    .addComponent(jTextFieldCnpj, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelInformaçõesGeraisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(RazaoSocial)
                    .addComponent(jTextFieldRazaoSocial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelInformaçõesGeraisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelInformaçõesGeraisLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jButtonAdicionar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonRemover, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabelTelefones)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedInformações.addTab("Informações Gerais", jPanelInformaçõesGerais);

        JLabelLogradouro.setText("Logradouro:");

        jLabelNumero.setText("Número:");

        jLabelBairro.setText("Bairro:");

        jTextFieldBairro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldBairroActionPerformed(evt);
            }
        });

        jLabelCidade.setText("Cidade:");

        jLabelEstado.setText("Estado:");

        jLabelPais.setText("País:");

        jComboBoxEstado.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "AC", "AL", "AP", "AM", "BA", "CE", "DF", "ES", "GO", "MA", "MT", "MS", "MG", "PA", "PB", "PR", "PE", "PI", "RJ", "RN", "RS", "RO", "RR", "SC", "SP", "SE", "TO" }));

        jLabelComplemento.setText("Complemento:");

        javax.swing.GroupLayout jPanelEnderecoLayout = new javax.swing.GroupLayout(jPanelEndereco);
        jPanelEndereco.setLayout(jPanelEnderecoLayout);
        jPanelEnderecoLayout.setHorizontalGroup(
            jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelEnderecoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanelEnderecoLayout.createSequentialGroup()
                        .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(JLabelLogradouro)
                            .addComponent(jLabelNumero)
                            .addComponent(jLabelBairro)
                            .addComponent(jLabelCidade)
                            .addComponent(jLabelEstado)
                            .addComponent(jLabelPais))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jComboBoxEstado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextFieldLogradouro, javax.swing.GroupLayout.PREFERRED_SIZE, 625, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextFieldNumero, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jTextFieldBairro, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 215, Short.MAX_VALUE)
                                .addComponent(jTextFieldCidade, javax.swing.GroupLayout.Alignment.LEADING))
                            .addComponent(jTextFieldPais, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanelEnderecoLayout.createSequentialGroup()
                        .addComponent(jLabelComplemento)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextFieldComplemento)))
                .addContainerGap(24, Short.MAX_VALUE))
        );
        jPanelEnderecoLayout.setVerticalGroup(
            jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelEnderecoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JLabelLogradouro)
                    .addComponent(jTextFieldLogradouro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelNumero)
                    .addComponent(jTextFieldNumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelBairro)
                    .addComponent(jTextFieldBairro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelCidade)
                    .addComponent(jTextFieldCidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelEstado)
                    .addComponent(jComboBoxEstado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelPais)
                    .addComponent(jTextFieldPais, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelComplemento)
                    .addComponent(jTextFieldComplemento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(35, Short.MAX_VALUE))
        );

        jTabbedInformações.addTab("Endereço", jPanelEndereco);

        jLabelNomeProduto.setText("Nome:");

        jLabelValorCompra.setText("Valor de Compra:");

        jLabelValorVenda.setText("Valor de Venda:");

        jLabelQuantidade.setText("Quantidade:");

        jLabelDescricao.setText("Descrição:");

        jLabelListaProdutos.setText("Lista de Produtos:");

        jListProdutos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jListProdutosMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(jListProdutos);

        jButtonAdicionarProduto.setText("+");
        jButtonAdicionarProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAdicionarProdutoActionPerformed(evt);
            }
        });

        jButtonRemoverProduto.setText("-");
        jButtonRemoverProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonRemoverProdutoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelProdutoLayout = new javax.swing.GroupLayout(jPanelProduto);
        jPanelProduto.setLayout(jPanelProdutoLayout);
        jPanelProdutoLayout.setHorizontalGroup(
            jPanelProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelProdutoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanelProdutoLayout.createSequentialGroup()
                        .addGroup(jPanelProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelValorCompra)
                            .addComponent(jLabelValorVenda)
                            .addComponent(jLabelQuantidade)
                            .addComponent(jLabelNomeProduto))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanelProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextFieldNomeProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 600, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanelProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jTextFieldValorCompra, javax.swing.GroupLayout.DEFAULT_SIZE, 75, Short.MAX_VALUE)
                                .addComponent(jTextFieldValorVenda)
                                .addComponent(jTextFieldQuantidade))))
                    .addGroup(jPanelProdutoLayout.createSequentialGroup()
                        .addGroup(jPanelProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelListaProdutos)
                            .addComponent(jLabelDescricao))
                        .addGroup(jPanelProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelProdutoLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanelProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jButtonAdicionarProduto, javax.swing.GroupLayout.DEFAULT_SIZE, 45, Short.MAX_VALUE)
                                    .addComponent(jButtonRemoverProduto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(jPanelProdutoLayout.createSequentialGroup()
                                .addGap(7, 7, 7)
                                .addComponent(jTextFieldDescricao)))))
                .addContainerGap(16, Short.MAX_VALUE))
        );
        jPanelProdutoLayout.setVerticalGroup(
            jPanelProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelProdutoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelNomeProduto)
                    .addComponent(jTextFieldNomeProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelValorCompra)
                    .addComponent(jTextFieldValorCompra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelValorVenda)
                    .addComponent(jTextFieldValorVenda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelQuantidade)
                    .addComponent(jTextFieldQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelDescricao)
                    .addComponent(jTextFieldDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(jPanelProdutoLayout.createSequentialGroup()
                        .addGroup(jPanelProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelProdutoLayout.createSequentialGroup()
                                .addComponent(jLabelListaProdutos)
                                .addGap(30, 30, 30))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelProdutoLayout.createSequentialGroup()
                                .addComponent(jButtonAdicionarProduto)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                        .addComponent(jButtonRemoverProduto)
                        .addGap(0, 13, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jTabbedInformações.addTab("Produto", jPanelProduto);

        jButtonSalvar.setText("Salvar");
        jButtonSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSalvarActionPerformed(evt);
            }
        });

        jButtonCancelar.setText("Cancelar");
        jButtonCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelarActionPerformed(evt);
            }
        });

        jButtonEditar.setText("Editar");
        jButtonEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEditarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jTabbedInformações)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabelListaFornecedores, javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(jButtonNovo, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonEditar, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonPesquisar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButtonCancelar)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonNovo)
                    .addComponent(jButtonExcluir)
                    .addComponent(jButtonPesquisar)
                    .addComponent(jButtonSalvar)
                    .addComponent(jButtonCancelar)
                    .addComponent(jButtonEditar))
                .addGap(18, 18, 18)
                .addComponent(jLabelListaFornecedores)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTabbedInformações)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonNovoActionPerformed

        umFornecedor = null;
        modoAlteracao = true;
        novoRegistro = true;
        this.limparCampos();
        this.habilitarDesabilitarCampos();
        this.jTextFieldNome.requestFocus();
         exibirInformacao("Selecione o tipo de fornecedor.");
        
    }//GEN-LAST:event_jButtonNovoActionPerformed

    private void jButtonExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonExcluirActionPerformed
       if(umFornecedor.getIdent().equals("PessoaFisica")) {   
        PessoaFisica pessoaFisica = (PessoaFisica) umFornecedor; 
        this.controleFornecedor.remover(pessoaFisica);
       }
       
       else {
        PessoaJuridica pessoaJuridica = (PessoaJuridica) umFornecedor;
         this.controleFornecedor.remover(pessoaJuridica);
       }
       
        umFornecedor = null;
        this.limparCampos();
        this.carregarListaFornecedores();
        this.habilitarDesabilitarCampos();
    }//GEN-LAST:event_jButtonExcluirActionPerformed

    private void jButtonCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelarActionPerformed
       if (novoRegistro == true) {
            this.limparCampos();
        } else {
            this.preencherCampos();
        }
        modoAlteracao = false;
        novoRegistro = false;
        this.habilitarDesabilitarCampos();
    }//GEN-LAST:event_jButtonCancelarActionPerformed

    private void jComboBoxFornecedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxFornecedorActionPerformed
        
        
         switch (jComboBoxFornecedor.getSelectedIndex()) {
            case PESSOA_FISICA_INDICE:
                jTextFieldCnpj.setEnabled(modoAlteracao == false);
                jTextFieldRazaoSocial.setEnabled(modoAlteracao == false);
                jTextFieldCpf.setEnabled(modoAlteracao == true);
                break;
            case PESSOA_JURIDICA_INDICE:
                jTextFieldCpf.setEnabled(modoAlteracao == false);
                jTextFieldCnpj.setEnabled(modoAlteracao == true);
                jTextFieldRazaoSocial.setEnabled(modoAlteracao == true);
                break;
            case 0:
                this.habilitarDesabilitarCampos();
                break;
            default:
                modoAlteracao = false;
                novoRegistro = false;
                this.habilitarDesabilitarCampos();
            }
    
    }//GEN-LAST:event_jComboBoxFornecedorActionPerformed

    private void jTextFieldBairroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldBairroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldBairroActionPerformed

    private void jButtonAdicionarProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdicionarProdutoActionPerformed
       produtosListModel.addElement(jTextFieldNomeProduto.getText());
       jTextFieldNomeProduto.setText(null);
    }//GEN-LAST:event_jButtonAdicionarProdutoActionPerformed

    private void jButtonSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalvarActionPerformed
        // TODO add your handling code here:
         this.salvarRegistro();
    }//GEN-LAST:event_jButtonSalvarActionPerformed

    private void jTableListaFornecedorMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableListaFornecedorMouseClicked
        // TODO add your handling code here:
        if (jTableListaFornecedor.isEnabled()) {
            DefaultTableModel model = (DefaultTableModel) jTableListaFornecedor.getModel();
            String nome = (String) model.getValueAt(jTableListaFornecedor.getSelectedRow(), 0);
            this.pesquisarFornecedor(nome);
            jButtonExcluir.setEnabled(true);
            jButtonEditar.setEnabled(true);
        }
    }//GEN-LAST:event_jTableListaFornecedorMouseClicked

    private void jButtonPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPesquisarActionPerformed
      String pesquisa = JOptionPane.showInputDialog("Informe o nome do Boxeador.");
    if (pesquisa != null) {
        this.pesquisarFornecedor(pesquisa);
    }
    }//GEN-LAST:event_jButtonPesquisarActionPerformed

    private void jButtonEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEditarActionPerformed
        modoAlteracao = true;
        novoRegistro = false;
        this.habilitarDesabilitarCampos();
        this.jTextFieldNome.requestFocus();
      
        
    }//GEN-LAST:event_jButtonEditarActionPerformed

    private void jButtonAdicionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdicionarActionPerformed
    CadastroTelefone cadastro = new CadastroTelefone(this, true);
    cadastro.setVisible(true);
    if (cadastro.getTelefone() != null) {
        telefonesListModel.addElement(cadastro.getTelefone());
    }
    cadastro.dispose();
    
    }//GEN-LAST:event_jButtonAdicionarActionPerformed

    private void jButtonRemoverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRemoverActionPerformed
       if (jListTelefones.getSelectedIndex() != -1) {
        telefonesListModel.removeElementAt(jListTelefones.getSelectedIndex());
    }
    }//GEN-LAST:event_jButtonRemoverActionPerformed

    private void jButtonRemoverProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRemoverProdutoActionPerformed
        if (jListProdutos.getSelectedIndex() != -1) {
        produtosListModel.removeElementAt(jListProdutos.getSelectedIndex());
    }
    }//GEN-LAST:event_jButtonRemoverProdutoActionPerformed

    private void jListProdutosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jListProdutosMouseClicked

    }//GEN-LAST:event_jListProdutosMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CadastroFornecedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CadastroFornecedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CadastroFornecedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CadastroFornecedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CadastroFornecedor().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel JLabelLogradouro;
    private javax.swing.JLabel RazaoSocial;
    private javax.swing.JButton jButtonAdicionar;
    private javax.swing.JButton jButtonAdicionarProduto;
    private javax.swing.JButton jButtonCancelar;
    private javax.swing.JButton jButtonEditar;
    private javax.swing.JButton jButtonExcluir;
    private javax.swing.JButton jButtonNovo;
    private javax.swing.JButton jButtonPesquisar;
    private javax.swing.JButton jButtonRemover;
    private javax.swing.JButton jButtonRemoverProduto;
    private javax.swing.JButton jButtonSalvar;
    private javax.swing.JComboBox jComboBoxEstado;
    private javax.swing.JComboBox jComboBoxFornecedor;
    private javax.swing.JLabel jLabelBairro;
    private javax.swing.JLabel jLabelCidade;
    private javax.swing.JLabel jLabelCnpj;
    private javax.swing.JLabel jLabelComplemento;
    private javax.swing.JLabel jLabelCpf;
    private javax.swing.JLabel jLabelDescricao;
    private javax.swing.JLabel jLabelEstado;
    private javax.swing.JLabel jLabelListaFornecedores;
    private javax.swing.JLabel jLabelListaProdutos;
    private javax.swing.JLabel jLabelNome;
    private javax.swing.JLabel jLabelNomeProduto;
    private javax.swing.JLabel jLabelNumero;
    private javax.swing.JLabel jLabelPais;
    private javax.swing.JLabel jLabelQuantidade;
    private javax.swing.JLabel jLabelTelefones;
    private javax.swing.JLabel jLabelTipoFornecedor;
    private javax.swing.JLabel jLabelValorCompra;
    private javax.swing.JLabel jLabelValorVenda;
    private javax.swing.JList jListProdutos;
    private javax.swing.JList jListTelefones;
    private javax.swing.JPanel jPanelEndereco;
    private javax.swing.JPanel jPanelInformaçõesGerais;
    private javax.swing.JPanel jPanelProduto;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTabbedPane jTabbedInformações;
    private javax.swing.JTable jTableListaFornecedor;
    private javax.swing.JTextField jTextFieldBairro;
    private javax.swing.JTextField jTextFieldCidade;
    private javax.swing.JTextField jTextFieldCnpj;
    private javax.swing.JTextField jTextFieldComplemento;
    private javax.swing.JTextField jTextFieldCpf;
    private javax.swing.JTextField jTextFieldDescricao;
    private javax.swing.JTextField jTextFieldLogradouro;
    private javax.swing.JTextField jTextFieldNome;
    private javax.swing.JTextField jTextFieldNomeProduto;
    private javax.swing.JTextField jTextFieldNumero;
    private javax.swing.JTextField jTextFieldPais;
    private javax.swing.JTextField jTextFieldQuantidade;
    private javax.swing.JTextField jTextFieldRazaoSocial;
    private javax.swing.JTextField jTextFieldValorCompra;
    private javax.swing.JTextField jTextFieldValorVenda;
    // End of variables declaration//GEN-END:variables

   

   
     
}
