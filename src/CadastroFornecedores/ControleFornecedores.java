
package CadastroFornecedores;

import java.util.ArrayList;


public class ControleFornecedores {
    
    private ArrayList<Fornecedor> listaFornecedores;
    
    public ControleFornecedores(){
       listaFornecedores = new ArrayList<Fornecedor>();
    }
    
        public String adicionar(PessoaFisica fornecedor){
            listaFornecedores.add(fornecedor);
            return "Fornecedor Pessoa Física adicionado com sucesso!";
        }
        
        public String adicionar(PessoaJuridica fornecedor){
            listaFornecedores.add(fornecedor);
            return "Fornecedor Pessoa Física adicionado com sucesso!";
        }
        
         public String remover(PessoaFisica fornecedor){
            listaFornecedores.remove(fornecedor);
            return "Fornecedor Pessoa Física removido com sucesso!";
        }
         
         public String remover(PessoaJuridica fornecedor){
            listaFornecedores.remove(fornecedor);
            return "Fornecedor Pessoa Jurídica removido com sucesso!";
        }
         
         public Fornecedor pesquisar(String umNome){
		for(Fornecedor Pessoa: listaFornecedores){
			if(Pessoa.getNome().equalsIgnoreCase(umNome)){
				return Pessoa;
			}
		}
		return null;
		}

    public ArrayList<Fornecedor> getListaFornecedor() {
        return listaFornecedores;
    }
        
         
         
         
}        
         


    
    